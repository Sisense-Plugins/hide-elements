

prism.run([function(){

	//	Define API call options (async=false to ensure it blocks html rendering)
	var options = {
		'url': '/api/users/loggedin',
		'method': 'GET',
		'async': 'false'
	}

	//	Make API call to get current user
	$.ajax(options)
		.done(function(response){
			//	Get the user's role
			var role = response.baseRoleName;
			if (!role){
				//	Nope, wait and try again
				console.log('hideElements Plugin: Error - no logged in user...weird')
				
			} else {
				
				//	All set, get the current user's mapping (based on prism.user.baseRoleName)
				var myMapping = mapping[role];
				if (myMapping){

					//	Loop through each classname
					myMapping.forEach(function(myDirective){

						//	Define the directive
						mod.directive(myDirective, [function(){
							return {
								'restrict': 'C',
								'link': function link($scope, element, attr){
									//	Hide the element
									$(element).css('visibility','hidden');
								}
							}
						}])
					})
				}
			}
		})
}])
